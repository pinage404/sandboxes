# Nix Sandboxes

Sandboxes / starters for different languages

[Get started](https://pinage404.gitlab.io/nix-sandboxes/)

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

## Do you like this project ?

<!-- markdownlint-disable-next-line MD045 -->
* If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
* If no, please [open an issue](https://gitlab.com/pinage404/nix-sandboxes/-/issues) to give your feedbacks [![open an issue](https://img.shields.io/gitlab/issues/all/pinage404%2Fnix-sandboxes?logo=gitlab)](https://gitlab.com/pinage404/nix-sandboxes/-/issues)

## Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/nix-sandboxes/-/issues), [merge request](https://gitlab.com/pinage404/nix-sandboxes/-/merge_requests) ...) are welcome

### Adding a language

Follow [instructions to add a language](https://pinage404.gitlab.io/nix-sandboxes/contributing.html#adding-a-language)

---

Fork of [`FaustXVI/sandboxes`](https://github.com/FaustXVI/sandboxes.git)
