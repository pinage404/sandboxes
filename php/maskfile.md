# Commands

## test

```sh
./vendor/bin/phpunit
```

### test watch

```sh
./vendor/bin/phpunit-watcher watch
```

## lint

```sh
phpstan analyse --level 5 ./src/
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
if [ -n "$NAME" ]; then
    bin/main "$NAME"
else
    bin/main
fi
```

## install

```sh
composer install
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
direnv exec . \
    composer update
```

---

<!-- markdownlint-disable-next-line MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
