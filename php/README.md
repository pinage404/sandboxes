# PHP with linting and test

[![Depfu](https://badges.depfu.com/badges/1b1cac5083cf613e098b58bbffdeff1b/count.svg)](https://depfu.com/gitlab/pinage404/nix-sandboxes?project_id=40506)

---

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=php/https://gitlab.com/pinage404/nix-sandboxes)

<details>
<summary>⚠️ Gitpod will open the root folder</summary>

Due to [some limitations of Gitpod](https://github.com/gitpod-io/gitpod/issues/5521), we cannot simply open a sub-folder

Opening in Gitpod will open the root folder

Two terminals will be opened :

1. the first in the root folder
2. the second in the target folder

Both terminals automatically load the environment of their current folder

![Screenshot of Gitpod showing two terminals open, the second being open in the target folder](https://gitlab.com/pinage404/nix-sandboxes/-/raw/main/gitpod.png)

</details>

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#php" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Learn PHP in Y Minutes](https://learnxinyminutes.com/docs/php/)

[Awesome PHP](https://github.com/ziadoz/awesome-php#readme)

[Learn Composer in Y Minutes](https://learnxinyminutes.com/docs/php-composer/)

[Awesome Composer](https://github.com/jakoch/awesome-composer#readme)

---

<!-- markdownlint-disable-next-line MD045 -->
[Read code on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes/-/tree/main/php)
