<?php

declare(strict_types=1);

namespace App\Console;

use function App\hello;

function main(array $arguments): void
{
    if (\count($arguments) === 2) {
        echo hello($arguments[1]).PHP_EOL;
    } else {
        echo hello().PHP_EOL;
    }
}
