<?php

declare(strict_types=1);

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use function App\hello;

final class HelloTest extends TestCase
{
    public function testHelloWorld(): void
    {
        $this->assertSame("Hello world", hello());
    }

    public function testHelloFoo(): void
    {
        $this->assertSame("Hello foo", hello("foo"));
    }
}
