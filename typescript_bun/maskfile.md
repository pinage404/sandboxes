# Commands

## test

```sh
bun test
```

### test watch

```sh
bun test --watch
```

## format

```sh
bun run format
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
bun run hello "$NAME"
```

## install

```sh
bun install
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
direnv exec . \
    bun update --latest
```

---

<!-- markdownlint-disable-next-line MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
