#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -o errtrace

list_templates() {
    nix flake show --json |
        jq --raw-output '.templates | keys_unsorted[]'
}

test_template() {
    local TEMPLATE="$1"
    local TEMP_DIR="${2:-/tmp}"

    # https://github.com/NixOS/nix/issues/8355#issuecomment-1551712655
    # https://github.com/NixOS/nix/issues/7154#issuecomment-1505656602
    unset TMPDIR

    nix flake new --quiet --template ".#$TEMPLATE" "$TEMP_DIR/$TEMPLATE"
    pushd "$TEMP_DIR/$TEMPLATE"
    direnv allow
    # shellcheck disable=SC2016
    direnv exec . sh -c 'eval $GAMBLE_TEST_COMMAND'
    popd
}

main() {
    local TEMP_DIR
    TEMP_DIR="$(mktemp --directory)"

    TEMPLATES="$(list_templates)"

    for TEMPLATE in $TEMPLATES; do
        test_template "$TEMPLATE" "$TEMP_DIR" "$@"
    done

    rm --recursive --force "$TEMP_DIR"
}

MAIN=${1:-main}
shift &>/dev/null || true

$MAIN "$@"
