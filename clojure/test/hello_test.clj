(ns hello-test
  (:require
   #_{:clj-kondo/ignore [:unused-referred-var]}
   [clojure.test :refer [deftest testing is are]]
   #_{:clj-kondo/ignore [:unused-referred-var :unused-namespace]}
   [debug.print :refer [pretty-print
                        spy
                        group-begin group-begin-collapsed group-end
                        print-group]]
   #_{:clj-kondo/ignore [:unused-referred-var :unused-namespace]}
   [debug.macro
    :refer [fn-input fn-output
            fn-io fn-group fn-group-io
            defn-io defn-group defn-group-io
            defn-generate-test]
    :refer-macros []]
   [hello :refer [hello]]))

(deftest hello-test
  (testing "world"
    (is (=
         "Hello world"
         (hello nil))))

  (testing "foo"
    (is (=
         "Hello foo"
         (hello "foo")))))
