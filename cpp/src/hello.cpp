#include "hello.h"
#include <format>

std::string hello(std::string_view name) {
  return std::format("Hello {}", name.empty() ? "world" : name);
}
