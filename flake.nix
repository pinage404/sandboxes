{
  description = "A basic flake with a shell and git-gamble";

  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  # nixConfig.extra-substituters = [
  #   "https://pinage404-nix-sandboxes.cachix.org"
  # ];
  # nixConfig.extra-trusted-public-keys = [
  #   "pinage404-nix-sandboxes.cachix.org-1:5zGRK2Ou+C27E7AdlYo/s4pow/w39afir+KRz9iWsZA="
  # ];

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";

        sandboxes-name = builtins.attrNames self.templates;
        SANDBOXES_NAME = "${nixpkgs.lib.strings.concatStringsSep ":" sandboxes-name}";

        shell_format_output = ''
          BOLD=$(tput bold)
          NORMAL=$(tput sgr0)
        '';

        sandboxes-list = nixpkgs.lib.attrsets.mapAttrsToList (
          name: value: "\${BOLD}${name}\${NORMAL}: ${value.description}"
        ) self.templates;
        SANDBOXES_LIST = "${nixpkgs.lib.strings.concatStringsSep "\n" sandboxes-list}";
      in
      {
        devShells.default = pkgs.mkShellNoCC {
          packages = [
            pkgs.devbox
          ];
        };
        devShells.direnv = pkgs.mkShellNoCC {
          packages = [
            pkgs.direnv
          ];
        };

        apps.default = self.apps."${system}".sandbox;
        apps.sandbox = {
          type = "app";
          program = "${nixpkgs.lib.getExe self.packages."${system}".default}";
        };

        packages.default = self.packages."${system}".sandbox;
        packages.sandbox = pkgs.writeShellApplication {
          name = "sandbox";
          text = ''
            SANDBOXES_NAME="${SANDBOXES_NAME}"

            ${shell_format_output}

            SANDBOXES_LIST="${SANDBOXES_LIST}"

            ${builtins.readFile ./use_sandbox.sh}
          '';
        };
        packages.sandboxes-name = pkgs.writeShellApplication {
          name = "sandboxes-name";
          text = ''
            echo "${SANDBOXES_NAME}"
          '';
        };
        packages.sandboxes-list = pkgs.writeShellApplication {
          name = "sandboxes-list";
          text = ''
            ${shell_format_output}

            echo "${SANDBOXES_LIST}"
          '';
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    )
    // {
      templates = {
        c = {
          description = "C with formatting and test";
          path = ./c;
        };

        clojure = {
          description = "Clojure with formatting, linting and test";
          path = ./clojure;
        };

        common_lisp = {
          description = "Common Lisp with formatting and test";
          path = ./common_lisp;
        };

        cpp = {
          description = "C++ with formatting and test";
          path = ./cpp;
        };

        elm = {
          description = "ELM with formatting, linting and test";
          path = ./elm;
        };

        erlang = {
          description = "Erlang with formatting and test";
          path = ./erlang;
        };

        forth = {
          description = "Forth with test";
          path = ./forth;
        };

        fsharp = {
          description = "F# with format, lint and test";
          path = ./fsharp;
        };

        go = {
          description = "Go with formatting and test";
          path = ./go;
        };

        haskell = {
          description = "Haskell with formatting, linting and test";
          path = ./haskell;
        };

        nix = {
          description = "Nix with formatting, linting and test";
          path = ./nix;
        };

        php = {
          description = "PHP with linting and test";
          path = ./php;
        };

        prolog = {
          description = "Prolog with test";
          path = ./prolog;
        };

        python = {
          description = "Python with formatting, typing and test";
          path = ./python;
        };
        python_poetry = {
          description = "Python with formatting, typing and test using Poetry (legacy name, will be removed)";
          path = ./python;
        };

        roc = {
          description = "Roc lang with formatting and test";
          path = ./roc;
        };

        rockstar = {
          description = "Rockstar with snapshot testing";
          path = ./rockstar;
        };

        ruby = {
          description = "Ruby with formatting, linting and test";
          path = ./ruby;
        };

        rust = {
          description = "Rust with formatting, linting and test";
          path = ./rust;
        };

        shell = {
          description = "Bash with formatting, linting and test";
          path = ./shell;
        };

        snapshot_testing = {
          description = "Small wrapper for snapshot testing";
          path = ./snapshot_testing;
        };

        typescript_bun = {
          description = "TypeScript with formatting and test on Bun";
          path = ./typescript_bun;
        };

        typescript_deno = {
          description = "TypeScript with formatting, linting and test on Deno";
          path = ./typescript_deno;
        };

        typescript_node_jest = {
          description = "TypeScript with formatting and test with Jest on NodeJS";
          path = ./typescript_node_jest;
        };

        typescript_node_vitest = {
          description = "TypeScript with formatting and test with Vitest on NodeJS";
          path = ./typescript_node_vitest;
        };
      };
    };
}
