package hello

import "fmt"

func HelloWorld(text string) string {
	if text == "" {
		text = "World"
	}
	return fmt.Sprintf("Hello, %s!", text)
}
