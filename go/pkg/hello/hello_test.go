package hello

import "testing"

func TestHelloWorld(test *testing.T) {
	expected := "Hello, World!"
	if observed := HelloWorld(""); observed != expected {
		test.Fatalf("HelloWorld() = %v, want %v", observed, expected)
	}
}

func TestHelloFoo(test *testing.T) {
	expected := "Hello, Foo!"
	if observed := HelloWorld("Foo"); observed != expected {
		test.Fatalf("HelloWorld() = %v, want %v", observed, expected)
	}
}
