package main

import (
	"fmt"
	"os"

	"gitlab.com/pinage404/nix-sandboxes/go/pkg/hello"
)

func main() {
	name := ""
	if len(os.Args) >= 2 {
		name = os.Args[1]
	}
	fmt.Println(hello.HelloWorld(name))
}
