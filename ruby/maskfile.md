# Commands

## test

```sh
rspec
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## lint

```sh
rubocop --autocorrect ./src/ ./spec/
```

## format

```sh
rubocop --fix-layout ./src/ ./spec/
```

## install

```sh
bundle install
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
direnv exec . \
    bundle update --all
```

---

<!-- markdownlint-disable-next-line MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
