$LOAD_PATH << 'src'

require 'hello'

describe 'hello', type: :feature do
  it 'without name says hello world' do
    expect(hello).to eq('Hello World!')
  end

  it 'with name says hello <name>' do
    expect(hello('Foo')).to eq('Hello Foo!')
  end
end
