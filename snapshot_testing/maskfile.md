# Commands

## test

```bash
set -o errexit -o nounset -o pipefail -o errtrace

$SNAPSHOT_TEST_COMMAND >./output/actual
delta --navigate --keep-plus-minus-markers --line-numbers --paging=never --true-color=never ./output/accepted ./output/actual
```

## accept

```sh
$SNAPSHOT_TEST_COMMAND >./output/accepted
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
$SNAPSHOT_TEST_COMMAND "$NAME"
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
```

---

<!-- markdownlint-disable-next-line MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
