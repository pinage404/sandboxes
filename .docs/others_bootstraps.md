# Others Bootstraps

List of alternatives bootstraps / setup / template with more languages

* [kata-bootstraps of Softwerkskammer Berlin](https://github.com/swkBerlin/kata-bootstraps)
* [List of setup](https://github.com/orgs/cyber-dojo-start-points/repositories?type=source)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [DevBox](https://www.jetify.com/devbox/) which is used by this repository
* [NixOS's templates](https://github.com/NixOS/templates)
* [DevEnv](https://devenv.sh/)
* [DevShell](https://numtide.github.io/devshell/)
* [Flox](https://floxdev.com/)
* [CodeWorks' Katapult](https://github.com/CodeWorksFrance/katapult)
* More on [Awesome Nix](https://github.com/nix-community/awesome-nix#development)
