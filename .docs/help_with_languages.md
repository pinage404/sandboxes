# Help with languages

List of tools to help writing in some language

* [Learn X in Y Minutes](https://learnxinyminutes.com)
* [Programming-Idioms](https://programming-idioms.org/)
* [Syntax Cheatsheet for Javascript/Python](https://www.theyurig.com/blog/javascript-python-syntax)
* [Awesome Lists](https://awesome.digitalbunker.dev/) curated lists of links related to a topic
* [Hello, World! in different languages on Wikipedia](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program#Examples)
* [Rosetta Code](https://www.rosettacode.org) same programs in many languages
* [The Algorithms](https://the-algorithms.com) many algorithms in many languages
