# Exercices / Katas

List of exercices / katas

* [Awesome Katas](https://github.com/gamontal/awesome-katas)
* [Kata Log](https://kata-log.rocks/index.html)
* [List of excercices](https://github.com/cyber-dojo/exercises-start-points/tree/master/start-points)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [Samman Technical Coaching's Katas](https://sammancoaching.org/kata_descriptions/index.html)
* [Emily Bache's Katas](https://github.com/emilybache?tab=repositories)
* [Coding Dojo's Katas](https://codingdojo.org/kata/)
* [Xavier Nopre's Katas](https://github.com/xnopre/xnopre-katas)
