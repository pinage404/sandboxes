# Sandboxes

Sandboxes / starters for different languages

It is useful for skipping most of the set-up phase ⏩ to :

* do a kata / exercise 🥋
* start a project 🏗️
* test something in an isolated environment 🧫

What is in the box ? 📦

<!-- markdownlint-disable MD033 -->
* With a focus on **reproducibility**
  <a href="https://builtwithnix.org"><img src="https://builtwithnix.org/badge.svg" alt="built with nix" style="vertical-align: middle;" /></a>

  It is possible to use it without Nix,
  but reproducibility cannot be guaranteed as you may have installed an incompatible version
* Always with **sample code** 🧩
* Always with **tests** 🧪
* Trying to use [**standard tools** depending on the ecosystem](./tool_comparison_table.md) 🧰
* Trying to have a **formatter** 🪄
* Trying to have a **linter** 🔎
<!-- markdownlint-enable -->

## Usage

### Online with GitPod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/nix-sandboxes)

### On you own machine

#### Requirements

* Install [Nix](https://nixos.org)
  * Preferably with [`nix-installer`](https://determinate.systems/nix-installer/)
* Install [DirEnv](https://direnv.net)
* Install [Git](https://git-scm.com)

#### List sandboxes

```sh
export NIX_CONFIG='extra-experimental-features = flakes nix-command'
nix run 'gitlab:pinage404/nix-sandboxes'
```

#### Get one sandbox

e.g. for Rust

```sh
export NIX_CONFIG='extra-experimental-features = flakes nix-command'
nix run 'gitlab:pinage404/nix-sandboxes' rust ./your_new_project_directory
```

## Optional steps

### VSCode

If you want to use VSCode

1. Install [VSCode](https://code.visualstudio.com)
1. Open from the command line in the folder

    ```sh
    code .
    ```

1. [Install recommanded extensions](https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_extension-recommendations)
     * Prefer a video ? Watch the [video tutorial](https://code.visualstudio.com/docs/introvideos/extend)

### Cache

To avoid rebuilding things that have already been built elsewhere, use Cachix

1. Install [Cachix](https://www.cachix.org/)
1. Use Nix's cache

    ```sh
    cachix use pinage404-nix-sandboxes
    ```

### Baby steps

<!-- markdownlint-disable MD033 -->
If you want to use [`git-gamble`](https://git-gamble.is-cool.dev)
<img src="https://git-gamble.is-cool.dev/assets/logo/git-gamble.svg" alt="" width="24" style="vertical-align: middle;" /> :
a tool that
blends [TDD (Test Driven Development)](https://en.wikipedia.org/wiki/Test-driven_development) + [TCR (`test && commit || revert`)](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864)
to make sure to **develop** the **right** thing 😌,
**baby step by baby step** 👶🦶
<!-- markdownlint-enable -->

The tool is already included right out of the box, and tests should pass

```sh
git gamble --pass
```
