# Tool comparison table

| Language                       | Platform     | Package Manager   | Formater         | Linter                            | Test Framework      |  Run   |
| :----------------------------- | :----------- | :---------------- | :--------------- | :-------------------------------- | :------------------ | :----: |
| [C]                            | [Clang]      |                   | [clang-format]   | [🤷🔎]                            | [Unity]             |   ✅   |
| [C++]                          | [Clang]      |                   | [clang-format]   | [🤷🔎]                            | [Catch2]            |   ✅   |
| [Clojure]                      | [jdk]        | [deps]            | [cljfmt]         | [clj-kondo], [eastwood], [splint] | [clojure.test]      |   ✅   |
| [Common Lisp]                  | [sbcl]       |                   | [lisp-format]    | [🤷🔎]                            | [FiveAM]            |   ✅   |
| [Elm]                          | [NodeJS]     | [elm-json]        | [elm-format]     | [elm-review]                      | [elm-test-rs]       |  [❌]  |
| [Erlang]                       |              | [rebar3]          | [erlfmt]         | [🤷🔎]                            | [EUnit]             |   ✅   |
| [Forth]                        | [gforth]     |                   | [🤷🪄]           | [🤷🔎]                            | [ffl]               |  [❌]  |
| [F#]                           | [DotNet]     | [DotNet]          | [Fantomas]       | [FSharpLint]                      | [xUnit]             |  [❌]  |
| [Go]                           |              |                   | [Go]             | [staticcheck]                     | [Go]                |   ✅   |
| [Haskell]                      | [GHC]        | [Cabal]           | [Fourmolu]       | [HLint]                           | [Hspec]             |   ✅   |
| [Nix]                          |              |                   | [nixpkgs-fmt]    | [statix], [nix-linter], [deadnix] | [Nixpkgs]           |   ✅   |
| [PHP]                          |              | [Composer]        | [🤷🪄]           | [PHPStan]                         | [PHPUnit]           |   ✅   |
| [Prolog]                       | [SWI-Prolog] |                   | [🤷🪄]           | [🤷🔎]                            | [Prolog Unit Tests] |   ✅   |
| [Python]                       |              | [Poetry]          | [Ruff]           | [Ruff], [mypy]                    | [pytest]            |   ✅   |
| [Roc]                          |              |                   | [Roc]            | [🤷🔎]                            | [Roc]               |  [❌]  |
| [Rockstar]                     |              |                   | [🤷🪄]           | [🤷🔎]                            | [Rockstar]          |  [❌]  |
| [Ruby]                         |              | [Bundler]         | [RuboCop]        | [RuboCop]                         | [RSpec]             |  [❌]  |
| [Rust]                         |              | [rustup], [Cargo] | [Rustfmt]        | [Clippy]                          | [speculoos]         |   ✅   |
| [Shell]                        | [Bash]       |                   | [shfmt]          | [ShellCheck]                      | [shUnit2]           |   ✅   |
| [Snapshot Testing]             |              |                   |                  |                                   | [delta]             |   ✅   |
| [TypeScript] [Bun]             | [Bun]        | [Bun]             | [Bun]            | [🤷🔎]                            | [Bun]               |   ✅   |
| [TypeScript] [Deno]            | [Deno]       | [Deno]            | [Deno]           | [Deno]                            | [Deno]              |   ✅   |
| [TypeScript] [NodeJS] [Jest]   | [ts-node]    | [PNPM]            | [Prettier]       | [🤷🔎]                            | [Jest]              |   ✅   |
| [TypeScript] [NodeJS] [Vitest] | [vite-node]  | [PNPM]            | [Prettier]       | [🤷🔎]                            | [Vitest]            |   ✅   |

[🤷🪄]: https://gitlab.com/pinage404/nix-sandboxes/-/issues/4
[🤷🔎]: https://gitlab.com/pinage404/nix-sandboxes/-/issues/5
[❌]: https://gitlab.com/pinage404/nix-sandboxes/-/issues/3

[C]: https://en.wikipedia.org/wiki/C_%28programming_language%29
[Clang]: https://clang.llvm.org
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[Unity]: https://github.com/ThrowTheSwitch/Unity/

[Clojure]: https://clojure.org
[jdk]: https://openjdk.java.net
[deps]: https://clojure.org/guides/deps_and_cli
[cljfmt]: https://github.com/weavejester/cljfmt
[clj-kondo]: https://github.com/clj-kondo/clj-kondo
[eastwood]: https://github.com/jonase/eastwood
[splint]: https://github.com/noahtheduke/splint
[clojure.test]: https://clojuredocs.org/clojure.test

[Common Lisp]: https://lisp-lang.org
[sbcl]: http://www.sbcl.org
[lisp-format]: https://github.com/eschulte/lisp-format
[FiveAM]: https://common-lisp-libraries.readthedocs.io/fiveam/

[C++]: https://en.cppreference.com/w/
[Clang]: https://clang.llvm.org
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[Catch2]: https://github.com/catchorg/Catch2

[Elm]: https://elm-lang.org
[NodeJS]: https://nodejs.org
[elm-json]: https://github.com/zwilias/elm-json
[elm-format]: https://github.com/avh4/elm-format
[elm-review]: https://github.com/jfmengels/elm-review
[elm-test-rs]: https://github.com/mpizenberg/elm-test-rs

[Erlang]: https://www.erlang.org
[rebar3]: https://rebar3.org
[erlfmt]: https://github.com/WhatsApp/erlfmt
[EUnit]: https://www.erlang.org/doc/apps/eunit/chapter.html

[Forth]: https://forth-standard.org
[gforth]: https://github.com/forthy42/gforth
[ffl]: https://irdvo.nl/FFL/

[F#]: https://fsharp.org
[DotNet]: https://dotnet.microsoft.com/en-us/
[Fantomas]: https://fsprojects.github.io/fantomas/
[FSharpLint]: https://fsprojects.github.io/FSharpLint/
[xUnit]: https://xunit.net

[Go]: https://go.dev
[staticcheck]: https://staticcheck.dev

[Haskell]: https://www.haskell.org
[GHC]: https://www.haskell.org/ghc/
[Cabal]: https://www.haskell.org/cabal/
[Fourmolu]: https://fourmolu.github.io/
[HLint]: https://github.com/ndmitchell/hlint
[Hspec]: https://hspec.github.io

[Nix]: https://nixos.org/manual/nix/stable/
[nixpkgs-fmt]: https://nix-community.github.io/nixpkgs-fmt/
[statix]: https://git.peppe.rs/languages/statix/about/
[nix-linter]: https://github.com/Synthetica9/nix-linter
[deadnix]: https://github.com/astro/deadnix
[Nixpkgs]: https://nixos.org/manual/nixpkgs/stable/#sec-functions-library-debug

[PHP]: https://www.php.net
[Composer]: https://getcomposer.org
[PHPStan]: https://phpstan.org
[PHPUnit]: https://phpunit.de

[Prolog]: https://en.wikipedia.org/wiki/Prolog
[SWI-Prolog]: https://www.swi-prolog.org
[Prolog Unit Tests]: https://www.swi-prolog.org/pldoc/doc_for?object=section%28%27packages/plunit.html%27%29

[Python]: https://www.python.org
[Poetry]: https://python-poetry.org
[Ruff]: https://astral.sh/ruff
[mypy]: https://www.mypy-lang.org
[pytest]: https://pytest.org

[Roc]: https://roc-lang.org

[Rockstar]: https://codewithrockstar.com/

[Ruby]: https://www.ruby-lang.org
[Bundler]: https://bundler.io
[RuboCop]: https://rubocop.org
[RSpec]: https://rspec.info

[Rust]: https://www.rust-lang.org
[rustup]: https://rustup.rs
[Cargo]: https://doc.rust-lang.org/cargo/
[Rustfmt]: https://rust-lang.github.io/rustfmt/
[Clippy]: https://doc.rust-lang.org/stable/clippy/index.html
[speculoos]: https://github.com/oknozor/speculoos

[Shell]: https://en.wikipedia.org/wiki/Shell_%28computing%29
[Bash]: https://www.gnu.org/software/bash/
[shfmt]: https://github.com/mvdan/sh
[ShellCheck]: https://www.shellcheck.net
[shUnit2]: https://github.com/kward/shunit2

[Snapshot Testing]: https://en.wikipedia.org/wiki/Software_testing#Output_comparison_testing
[delta]: https://dandavison.github.io/delta/

[TypeScript]: https://www.typescriptlang.org/
[Bun]: https://bun.sh

[Deno]: https://deno.com

[PNPM]: https://pnpm.io
[Prettier]: https://prettier.io
[ts-node]: https://typestrong.org/ts-node/
[Jest]: https://jestjs.io

[vite-node]: https://github.com/vitest-dev/vitest/tree/main/packages/vite-node
[Vitest]: https://vitest.dev

<style>
    :root {
        --content-max-width: 100%;
    }

    @media (min-width: 1081px) {
        .sidebar-hidden {
            --content-max-width: calc(100% - 90px * 2);
        }
    }

    @media (min-width: 1381px) {
        :root {
            --content-max-width: calc(100% - 90px * 2);
        }
    }
</style>
