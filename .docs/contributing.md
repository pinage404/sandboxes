# Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/nix-sandboxes/-/issues), [merge request](https://gitlab.com/pinage404/nix-sandboxes/-/merge_requests) ...) are welcome

## Adding a language

### Commit message format

Commit message should be `<language>: <a descriptive message>`

* prefix your commit with
  * `<language>:` the name of the folder
  * `all:` when (almost) every languages are touched
  * `meta:` when it is about the project itself
  * `base:` when it touch the root folder

### Steps

1. copy an existing folder (e.g. `shell`)
1. name the folder with the language
1. adapt it
   * in `<language>/flake.nix`, in the `packages`, add every dependencies needed
   * `mask <target>` must return 0 code on success, non 0 on fail
   * complete the `<language>/maskfile.md` with
     * `mask test`
     * `mask lint` if possible
     * `mask format` if possible
     * `mask run` if possible
       * `mask run` should display `Hello world`
       * `mask run --name foo` should display `Hello foo`
     * `mask install` if there is specific steps to do manually such as pulling dependencies
     * `mask update` if there is specific steps to update the language and dependencies
   * in `<language>/.config/git/hooks/pre-gamble`, keep
     1. `mask format` if possible
     1. `mask lint` if possible
   * in `<language>/flake.nix`, change the `description`
   * in `<language>/README.md`, change the title
   * in `<language>/README.md`, change `<language>`
     * in the URL
     * in the flake command
1. add an usage example of the language
   * using TDD
     * or even better using [TCRDD](https://git-gamble.is-cool.dev/theory.html#tcrdd) (e.g. using [`git-gamble`](https://git-gamble.is-cool.dev))
   * `hello` without argument or an optionnal (none) or empty string (depending on the language possibility) should output `Hello world`
   * `hello` with `foo` argument as string should output `Hello foo`
   * if you are a beginner with this language you may need to look at [help with language](./help_with_languages.md)
1. in `flake.nix`, in the `templates` section, add the new template
1. in `.gitlab-ci.yml`, in the `LANGUAGE` list, add the new language

Put the same description in

* `flake.nix`'s `description`
* `<language>/flake.nix`'s `description`
* `<language>/README.md` title

### VSCode support

* in `<language>/.vscode/extension.json`, add needed extensions

### Document it

* in [`.docs/tool_comparison_table.md`](./tool_comparison_table.md) table, add a row for the new language
* add a symbolic link between the `<language>/README.md` and the documentation

    ```sh
    ln -s ../../<language>/README.md ./.docs/sandboxes/<language>.md
    ```

* in `.docs/SUMMARY.md`, add a markdwon link to `./sandboxes/<language>.md`
