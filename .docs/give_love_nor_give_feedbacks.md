# Give Love ❤️ nor Give Feedbacks 🗣️

## Do you like this project ?

<!-- markdownlint-disable-next-line MD045 -->
- If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
- If no, please [open an issue](https://gitlab.com/pinage404/nix-sandboxes/-/issues) to give your feedbacks [![open an issue](https://img.shields.io/gitlab/issues/all/pinage404%2Fnix-sandboxes?logo=gitlab)](https://gitlab.com/pinage404/nix-sandboxes/-/issues)
