#!/usr/bin/env sh

hello() {
	NAME="${1:-world}"
	echo "Hello ${NAME}"
}
