#!/usr/bin/env sh

test_hello_world() {
	. ./src/hello.sh
	assertEquals "$(hello)" "Hello world"
}

test_hello_foo() {
	. ./src/hello.sh
	assertEquals "$(hello 'foo')" "Hello foo"
}
