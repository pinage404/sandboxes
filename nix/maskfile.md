# Commands

## test

```sh
nix eval .#tests
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## lint

```bash
set -o errexit -o nounset -o pipefail -o errtrace

statix fix
nix-linter --check=BetaReduction --check=EmptyVariadicParamSet --check=UnneededAntiquote ./**/**.nix
deadnix --no-underscore --fail
```

## format

```sh
nix fmt **.nix
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```bash
set -o errexit -o nounset -o pipefail -o errtrace

module="./src/hello.nix"
module_arguments="{}"

hello_function="(builtins.import $module) $module_arguments"

hello_arguments="{}"
if [ -n "$NAME" ]; then
    hello_arguments="{ name = \"$NAME\"; }"
fi

nix eval \
    --expr "$hello_function $hello_arguments" \
    --impure `# allow to use the builtins.import` \
    --raw `# remove the quotes around string` \
    --read-only # do not instantiate each evaluated derivation
echo # end with a new line
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    devbox update
```

---

<!-- markdownlint-disable-next-line MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
