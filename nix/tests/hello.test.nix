{
  lib,
  nixpkgs-lib,
}:

nixpkgs-lib.runTests {
  test_hello_world = {
    expr = lib.hello { };
    expected = "Hello world";
  };

  test_hello_foo = {
    expr = lib.hello { name = "foo"; };
    expected = "Hello foo";
  };
}
