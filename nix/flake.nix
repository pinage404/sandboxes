{
  description = "Nix with formatting, linting and test";

  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  # nixConfig.extra-substituters = [
  #   "https://pinage404-nix-sandboxes.cachix.org"
  # ];
  # nixConfig.extra-trusted-public-keys = [
  #   "pinage404-nix-sandboxes.cachix.org-1:5zGRK2Ou+C27E7AdlYo/s4pow/w39afir+KRz9iWsZA="
  # ];

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.nixpkgs-lib.url = "github:nix-community/nixpkgs.lib";

  inputs.haumea.url = "github:nix-community/haumea";
  inputs.haumea.inputs.nixpkgs.follows = "nixpkgs-lib";

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      nixpkgs-lib,
      haumea,
    }:
    (flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShells.default = pkgs.mkShellNoCC {
          packages = [
            pkgs.devbox
          ];
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    ))
    // {
      tests =
        let
          results = haumea.lib.load {
            src = ./tests;
            inputs = {
              nixpkgs-lib = nixpkgs-lib.lib;
              inherit (self) lib;
            };
          };
          passed = builtins.concatLists (builtins.attrValues results) == [ ];
        in
        if passed then "all tests passed" else throw (nixpkgs-lib.lib.generators.toPretty { } results);

      lib = haumea.lib.load {
        src = ./src;
      };
    };
}
