{
  lib,
  stdenv,
  fetchzip,
  autoPatchelfHook,
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "rockstar";
  version = "2.0.29";

  src = fetchzip {
    url = "https://github.com/RockstarLang/rockstar/releases/download/v${finalAttrs.version}/rockstar-v${finalAttrs.version}-linux-x64.tar.gz";
    hash = "sha256-sUy4UhjAs5ZsM5zophE2tlh0GLGl6WPtswPmNVCuIqY=";
    stripRoot = false;
  };

  nativeBuildInputs = [
    autoPatchelfHook
  ];

  installPhase = ''
    install -D rockstar-linux-x64-binary/rockstar $out/bin/rockstar
  '';

  meta = {
    description = "Home of the Rockstar programming language";
    homepage = "https://github.com/RockstarLang/rockstar";
    license = lib.licenses.agpl3Only;
    mainProgram = "rockstar";
    platforms = [ "x86_64-linux" ];
    sourceProvenance = [ lib.sourceTypes.binaryNativeCode ];
  };
})
