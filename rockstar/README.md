# Rockstar with snapshot testing

Also known as [Golden Master Testing](https://en.wikipedia.org/wiki/Software_testing#Output_comparison_testing)

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=rockstar/https://gitlab.com/pinage404/nix-sandboxes)

<details>
<summary>⚠️ Gitpod will open the root folder</summary>

Due to [some limitations of Gitpod](https://github.com/gitpod-io/gitpod/issues/5521), we cannot simply open a sub-folder

Opening in Gitpod will open the root folder

Two terminals will be opened :

1. the first in the root folder
2. the second in the target folder

Both terminals automatically load the environment of their current folder

![Screenshot of Gitpod showing two terminals open, the second being open in the target folder](https://gitlab.com/pinage404/nix-sandboxes/-/raw/main/gitpod.png)

</details>

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#rockstar" ./your_new_project_directory
```

Limitation : it has only be packaged for Linux x86_64 at the moment

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Rockstar](https://codewithrockstar.com/)

[Tutorial](https://codewithrockstar.com/tutorial)

[Documentation](https://codewithrockstar.com/docs/)

[Esolang wiki](https://esolangs.org/wiki/Rockstar)

---

<!-- markdownlint-disable-next-line MD045 -->
[Read code on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes/-/tree/main/rockstar)
