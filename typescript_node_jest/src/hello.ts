export const hello = (name = "world"): string => {
  return `Hello ${name}`
}
