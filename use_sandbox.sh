#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -o errtrace

export NIX_CONFIG='extra-experimental-features = flakes nix-command'

DRY_RUN=''

echo_bold() {
  INPUT="$1"
  BOLD="$(tput bold)"
  NORMAL="$(tput sgr0)"

  echo "$BOLD$INPUT$NORMAL"
}

use_sandbox() {
  SANDBOX="$1"
  DESTINATION_DIRECTORY="$2"

  if [[ "$DRY_RUN" == "" ]]; then
    set -o xtrace
  fi

  if command -v cachix; then
    $DRY_RUN cachix use pinage404-nix-sandboxes
  fi
  $DRY_RUN nix flake new --template "gitlab:pinage404/nix-sandboxes#$SANDBOX" "$DESTINATION_DIRECTORY"
  $DRY_RUN direnv allow "$DESTINATION_DIRECTORY"
  $DRY_RUN cd "$DESTINATION_DIRECTORY" || exit 1
  if [[ "$VISUAL" != "" ]]; then
    # shellcheck disable=SC2086
    $DRY_RUN $VISUAL .
  elif [[ "$EDITOR" != "" ]]; then
    # shellcheck disable=SC2086
    $DRY_RUN $EDITOR .
  fi
}

display_help() {
  ENABLE_FLAKE="NIX_CONFIG=$NIX_CONFIG"

  SANDBOXES_LIST="${SANDBOXES_LIST:-$(nix run 'gitlab:pinage404/nix-sandboxes#sandboxes-list')}"

  echo_bold "## Usage"
  echo "$ENABLE_FLAKE \\"
  echo "nix run 'gitlab:pinage404/nix-sandboxes' -- [-h | --help] [-n | --dry-run] [<SANDBOX> [<PATH>]]"
  echo ""
  echo_bold "### Exemple"
  echo "$ENABLE_FLAKE \\"
  echo "nix run 'gitlab:pinage404/nix-sandboxes' -- rust ./your_new_project_directory"
  echo ""
  echo_bold "## Sandboxes list"
  echo -e "${SANDBOXES_LIST}"
}

SANDBOX=""
DESTINATION_DIRECTORY="${2:-./your_new_project_directory}"

SANDBOXES_NAME="${SANDBOXES_NAME:-$(nix run 'gitlab:pinage404/nix-sandboxes#sandboxes-name')}"

for ARG in "$@"; do
  if [[ ":$SANDBOXES_NAME:" =~ :$ARG: ]]; then
    SANDBOX="$ARG"
  else
    case "$ARG" in
    -h | --help)
      display_help
      exit 0
      ;;

    -n | --dry-run)
      DRY_RUN='echo'
      ;;

    *)
      DESTINATION_DIRECTORY="$ARG"
      ;;
    esac
  fi
  shift
done

if [[ "$SANDBOX" = "" ]]; then
  display_help
  exit 1
fi

use_sandbox "$SANDBOX" "$DESTINATION_DIRECTORY"
